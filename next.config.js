const withImages = require("next-images");
module.exports = withImages({
  webpack(config, options) {
    config.module.rules.push({
      test: /\.csv$/i,
      use: "raw-loader"
    });
    return config;
  }
});
