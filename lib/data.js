import rv045Csv from "../assets/RV045.csv";
import rv0451Csv from "../assets/RV0451.csv";
import rv0452Csv from "../assets/RV0452.csv";
import rv0453Csv from "../assets/RV0453.csv";
import rv0454Csv from "../assets/RV0454.csv";

const CSV_MAP = {
  RV045: rv045Csv,
  RV0451: rv0451Csv,
  RV0452: rv0452Csv,
  RV0453: rv0453Csv,
  RV0454: rv0454Csv
};

const cleanQuotes = str => str.replace('"', "").replace('"', "");

export const ageDataset = { file: "RV045", sexDim: "DIM1", ageDim: "DIM2" };
export const locTypeDataset = {
  file: "RV0451",
  sexDim: "DIM3",
  ageRangeDim: "DIM4",
  specialDim: "DIM2"
};
export const locDataset = {
  file: "RV0452",
  sexDim: "DIM3",
  ageRangeDim: "DIM4",
  specialDim: "DIM2"
};
export const natDataset = {
  file: "RV0453",
  sexDim: "DIM3",
  ageRangeDim: "DIM4",
  specialDim: "DIM2"
};
export const eduDataset = {
  file: "RV0454",
  sexDim: "DIM3",
  ageRangeDim: "DIM4",
  specialDim: "DIM2"
};

export const datasets = [
  eduDataset,
  natDataset,
  locDataset,
  locTypeDataset,
  ageDataset
];

export const fetchDataset = async datasetCode => {
  const result = [];
  const tsv = CSV_MAP[datasetCode];
  const lines = tsv.split("\n");

  let keys = null;
  let keyLabels = null;
  for (let line of lines) {
    const lineSplit = line.split("|");
    if (lineSplit.length <= 1) {
      continue;
    }

    if (!keys) {
      keys = [];
      keyLabels = {};
      for (let codeLabel of lineSplit) {
        let key = cleanQuotes(codeLabel);
        let label = key;
        if (key.includes(":")) {
          const keySplit = key.split(":");
          key = keySplit[0];
          label = keySplit[1];
        }
        key = key.trim();
        keys.push(key);
        keyLabels[key] = label;
      }
      continue;
    }

    const lineObj = {};
    for (let i = 0; i < keys.length; i++) {
      const valKey = keys[i];
      let val = cleanQuotes(lineSplit[i]);
      if (val.includes(":")) {
        val = val.split(":")[1];
      }
      lineObj[valKey] = val;
    }
    result.push(lineObj);
  }

  return [keyLabels, result];
};
