import { Global, css } from "@emotion/core";

export default (
  <Global
    styles={css`
      @import url("https://use.typekit.net/rmp4bgn.css");

      * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
      }

      a {
        color: #33295c;
        text-decoration: none;
      }

      html {
        color: #33295c;
        background-color: white;
        font-family: "houschka-rounded", sans-serif;
        font-size: 18px;
        font-weight: 500;
      }

      #__next {
        display: flex;
        width: 100vw;
        justify-content: center;
      }
    `}
  />
);
