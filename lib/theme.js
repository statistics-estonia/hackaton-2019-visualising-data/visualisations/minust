export default {
  fonts: {
    primary: "houschka-rounded, sans-serif"
  },
  colors: {
    white: "white",
    purple: "#33295C",
    purpleRgb: "51, 41, 92",
    green: "#98D36E",
    red: "#fb5876"
  },
  shadows: {},
  borders: {},
  bezier: {}
};
