import React, { useState } from "react";
import { useSpring, animated } from "react-spring";
import styled from "@emotion/styled";

const Wrapper = styled(animated.div)`
  flex-shrink: 0;
  display: inline-flex;
  width: calc(50vh - 160px);
  height: calc(50vh - 160px);
  background-color: transparent;
  border: 2px solid #33295c;
  border-radius: 24px;

  flex-direction: column;
  align-items: center;
  justify-content: center;

  margin-left: 32px;

  font-size: 32px;
  user-select: none;
`;

const ArrowSvg = styled(animated.svg)`
  margin-bottom: 24px;
`;

const Arrow = ({ stroke }) => (
  <ArrowSvg
    width="98"
    height="64"
    xmlns="http://www.w3.org/2000/svg"
    stroke={stroke}
    strokeWidth="2"
    fill="none"
    fillRule="evenodd"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <path d="M1.1 32h95.6M63.6 1l33 31-33 31" />
  </ArrowSvg>
);

const FullscreenOverlay = styled(animated.div)`
  z-index: 1000;
  background-color: #33295c;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export default ({ disabled, results }) => {
  const wrapperStyle = useSpring({
    backgroundColor: disabled ? "white" : "#33295C",
    color: disabled ? "#33295C" : "white",
    config: { mass: 5, tension: 750, friction: 100 }
  });

  const { stroke } = useSpring({
    stroke: disabled ? "#33295C" : "white",
    config: { mass: 5, tension: 750, friction: 100 }
  });

  const [navigating, setNavigating] = useState(false);

  const navigate = () => {
    setNavigating(true);
    setTimeout(() => {
      window.location = `/results?${results}`;
    }, 750);
  };

  const overlayStyle = useSpring({
    opacity: navigating ? 1 : 0,
    config: { mass: 5, tension: 750, friction: 100 }
  });

  return (
    <Wrapper
      style={{ ...wrapperStyle, cursor: disabled ? "auto" : "pointer" }}
      onClick={disabled || navigating ? null : navigate}
    >
      <Arrow stroke={stroke} />
      Results
      <FullscreenOverlay
        style={{ ...overlayStyle, pointerEvents: navigating ? "auto" : "none" }}
      />
    </Wrapper>
  );
};
