import React from "react";
import styled from "@emotion/styled";
import { ThemeProvider } from "emotion-theming";
import theme from "../lib/theme";
import globalStyles from "../lib/global";

const Wrapper = styled.div`
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export default ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      <Wrapper>
        {globalStyles}
        {children}
      </Wrapper>
    </ThemeProvider>
  );
};
