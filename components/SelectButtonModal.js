import React from "react";
import styled from "@emotion/styled";
import { animated, useSpring, useTrail } from "react-spring";
import { useMeasure } from "../lib/helpers";

const Wrapper = styled(animated.div)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  display: flex;
  flex-direction: column;
  align-items: stretch;

  background-color: rgba(255, 255, 255, 1);
  border-radius: 24px;

  font-size: 2em;
  z-index: 1;

  user-select: none;
  overflow: hidden;
`;

const Content = styled.div`
  flex: 1 1 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: 24px;
  padding: 20px 0;

  > * {
    margin: 12px 0;
    color: #6c638f;
    transition: color 0.5s cubic-bezier(0.3, 1, 0.3, 1);

    &:hover {
      color: #33295c;
    }
  }
`;

export default ({ open, options, onSelect, wrapperHeight }) => {
  const longList = options.length > 50;

  const trail = useTrail(options.length, {
    opacity: open ? 1 : 0,
    y: open ? 0 : -32,
    config: {
      mass: 5,
      tension: longList ? 15000 : 2000,
      friction: longList ? 500 : 125
    }
  });

  const [bindContent, { height: contentHeight }] = useMeasure();

  let topOffset = 0;
  let scrollable = false;
  if (open && contentHeight > wrapperHeight) {
    topOffset = Math.max(contentHeight - wrapperHeight, 0);
    if (topOffset > wrapperHeight * 1.5) {
      topOffset = wrapperHeight * 1.5;
      scrollable = true;
    }
  }

  const { opacity, top, pointerEvents } = useSpring({
    opacity: open ? 1 : 0,
    top: open ? -topOffset : 0,
    pointerEvents: open ? "auto" : "none",
    config: { mass: 5, tension: 750, friction: 100 }
  });

  return (
    <Wrapper
      style={{
        opacity,
        pointerEvents,
        top,
        overflowY: scrollable ? "auto" : "none"
      }}
    >
      <Content {...bindContent}>
        {trail.map(({ y, ...props }, idx) => {
          const option = options[idx];
          return (
            <animated.div
              style={{
                ...props,
                transform: y.interpolate(y => `translate3d(0, ${y}px, 0)`),
                textAlign: "center",
                cursor: "pointer"
              }}
              key={option.value}
              onClick={() => onSelect && onSelect(option)}
            >
              {option.label}
            </animated.div>
          );
        })}
      </Content>
    </Wrapper>
  );
};
