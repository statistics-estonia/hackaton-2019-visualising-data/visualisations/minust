import React, { useState } from "react";
import { animated, useSpring } from "react-spring";
import styled from "@emotion/styled";
import SelectButtonModal from "./SelectButtonModal";
import { useMeasure } from "../lib/helpers";

const Wrapper = styled(animated.div)`
  flex-shrink: 0;
  position: relative;
  display: inline-flex;
  width: calc(50vh - 160px);
  height: calc(50vh - 160px);
  background-color: transparent;
  border: 2px solid #33295c;
  border-radius: 24px;

  flex-direction: column;
  align-items: center;
  justify-content: center;

  margin-left: 32px;
  &:first-of-type {
    margin-left: 0;
  }
  user-select: none;
`;

const Backdrop = styled(animated.div)`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(51, 41, 92, 0.9);
  z-index: 0;
`;

const Label = styled(animated.div)`
  position: absolute;
  height: 72px;
  background-color: #33295c;
  bottom: -1px;
  left: -1px;
  right: -1px;
  border: 2px solid #33295c;
  border-radius: 24px;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  color: white;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: 24px;
`;

const Value = styled.div`
  position: absolute;
  top: 72px;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-size: 32px;
  text-align: center;
`;

const Icon = styled.img`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 80px;
  height: 80px;
  transform: translate(-50%, -75%);
`;

export default ({ options, onSelect, label, value, iconPath }) => {
  const [selectOpen, setSelectOpen] = useState(false);

  const config = { mass: 5, tension: 750, friction: 100 };

  const wrapperStyle = useSpring({
    borderColor: value ? "#98D36E" : "#33295C",
    config
  });

  const backdropStyle = useSpring({
    opacity: selectOpen ? 1 : 0,
    pointerEvents: selectOpen ? "auto" : "none",
    config
  });

  const [bindWrapper, { height: wrapperHeight }] = useMeasure();

  const labelSpringProps = {
    backgroundColor: value ? "#98D36E" : "#33295C",
    borderColor: value ? "#98D36E" : "#33295C",
    borderTopLeftRadius: value ? "24px" : "8px",
    borderTopRightRadius: value ? "24px" : "8px",
    borderBottomLeftRadius: value ? "8px" : "24px",
    borderBottomRightRadius: value ? "8px" : "24px",
    config
  };

  if (wrapperHeight) {
    labelSpringProps.top = value ? "-1px" : `${wrapperHeight - 71}px`;
    labelSpringProps.bottom = value ? `${wrapperHeight - 71}px` : "-1px";
  }

  const labelStyle = useSpring(labelSpringProps);

  return (
    <Wrapper
      onClick={() => setSelectOpen(!selectOpen)}
      style={{
        ...wrapperStyle,
        zIndex: selectOpen ? 10 : "auto",
        cursor: selectOpen ? "auto" : "pointer"
      }}
      {...bindWrapper}
    >
      {value ? <Value>{value.label}</Value> : <Icon src={iconPath} />}
      <Label style={labelStyle}>{label}</Label>
      <Backdrop style={backdropStyle} />
      <SelectButtonModal
        open={selectOpen}
        options={options}
        onSelect={onSelect}
        wrapperHeight={wrapperHeight}
      />
    </Wrapper>
  );
};
