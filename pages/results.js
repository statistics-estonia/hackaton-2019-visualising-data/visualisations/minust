import React, { useRef } from "react";
import { animated, useSpring } from "react-spring";
import styled from "@emotion/styled";
import PageWrapper from "../components/PageWrapper";
import logoSvg from "../assets/Logo_White.svg";
import { ageDataset, eduDataset, fetchDataset, locDataset } from "../lib/data";
import useScrollSpy from "../lib/scrollspy";

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1 1 auto;
  align-self: stretch;
`;

const Left = styled(animated.div)`
  width: 50vw;
  background-color: #33295c;
  color: white;
  overflow: auto;
  z-index: 1;
`;

const LeftContent = styled(animated.div)`
  display: flex;
  flex-direction: column;
  padding: 80px;
`;

const Logo = styled.img`
  width: 112px;
  height: 24px;
  cursor: pointer;
`;

const ResultWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 148px;
  margin-bottom: 512px;
`;

const ResultHeader = styled.div`
  font-size: 32px;
  line-height: 38px;
  margin-bottom: 48px;
`;

const ResultBlock = styled.div`
  position: relative;
  height: 92px;
  border: 2px solid ${p => (p.green ? "#98D36E" : p.red ? "#fb5876" : "white")};
  border-radius: 24px;
  margin: 8px 0;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: 24px;
  padding-right: 50%;
  font-size: 24px;
`;
const ResultBlockHighlight = styled.div`
  position: absolute;
  top: -1px;
  left: ${p => (p.smaller ? 75 : 50)}%;
  right: -1px;
  bottom: -1px;
  background-color: ${p => (p.green ? "#98D36E" : p.red ? "#fb5876" : "white")};
  color: #33295c;
  border-radius: 24px;
  border-top-left-radius: 12px;
  border-bottom-left-radius: 12px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-size: 24px;
`;

const Right = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  width: 50vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
`;

const AgeTimeline = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  flex: 1 1 auto;
  display: flex;
  flex-direction: row;
`;
const AgeBlock = styled(animated.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${p =>
    p.bluish ? "#ddd8f5" : p.greenish ? "#98d36e" : "white"};
  overflow: hidden;
  text-align: center;
`;

const Results = ({
  sex,
  age,
  ageResult,
  ageResultOpposite,
  loc,
  locResult,
  locResultLow,
  locResultHigh,
  locResultAvg,
  edu,
  eduResult,
  eduResultLow,
  eduResultHigh
}) => {
  const leftStyle = useSpring({
    from: { width: "100vw" },
    width: "50vw",
    config: { mass: 5, tension: 750, friction: 100 }
  });
  const leftContentStyle = useSpring({
    from: { opacity: 0 },
    opacity: 1,
    delay: 500,
    config: { mass: 5, tension: 500, friction: 100 }
  });

  const num = val => parseFloat(val);
  const round = val => val.toFixed(2).replace(".00", "");
  const roundFull = val => Math.round(val);

  if (!ageResult) {
    return <div>no</div>;
  }

  const sexSingular = sex === "Mehed" ? "mees" : "naine";
  const oppositeSex = sex === "Mehed" ? "Women" : "Men";
  const oppositeAgeResultDiff = num(ageResult) - num(ageResultOpposite);
  const sexGreenRedProps = {
    green: oppositeAgeResultDiff > 0,
    red: oppositeAgeResultDiff < 0
  };
  const sexPredictedAge = num(ageResult) + num(age);
  const sexPredictedAgePerc = num(age) / sexPredictedAge;

  const locResultLowDiff = roundFull(num(locResult) - num(locResultLow.Value));
  const locResultLowProps = {
    green: locResultLowDiff < 0,
    red: locResultLowDiff > 0
  };
  const locResultHighDiffNum = num(locResult) - num(locResultHigh.Value);
  const locResultHighDiff = roundFull(locResultHighDiffNum);
  const locResultHighProps = {
    green: locResultHighDiff < 0,
    red: locResultHighDiff > 0
  };
  const locResultAvgDiff = num(locResult) - locResultAvg;
  let avgCopy = `People arond here live  ${roundFull(
    Math.abs(locResultAvgDiff)
  )} years ${
    locResultAvgDiff > 0
      ? "longer than the national average."
      : "less than the national average."
  }`;
  if (Math.abs(locResultAvgDiff) < 0.5) {
    avgCopy = "People around here live the average lifespan.";
  }

  const eduResultLowDiff = roundFull(num(eduResult) - num(eduResultLow.Value));
  const eduResultLowProps = {
    green: eduResultLowDiff < 0,
    red: eduResultLowDiff > 0
  };
  const eduResultHighDiffNum = num(eduResult) - num(eduResultHigh.Value);
  const eduResultHighDiff = roundFull(eduResultHighDiffNum);
  const eduResultHighProps = {
    green: eduResultHighDiff < 0,
    red: eduResultHighDiff > 0
  };

  const sectionRefs = [useRef(null), useRef(null), useRef(null)];

  const activeSectionIdx = useScrollSpy({
    sectionElementRefs: sectionRefs,
    offsetPx: -512
  });

  let highDiff =
    Math.abs(locResultHighDiffNum) + Math.abs(eduResultHighDiffNum); // years to gain

  let percPool = sexPredictedAge; // total predicted years to live (without gain)
  if (activeSectionIdx > 0) {
    percPool += highDiff;
  }

  let firstBlockPerc = num(age) / percPool;
  let secondBlockPerc = num(ageResult) / percPool;
  let thirdBlockPerc = 0;
  if (activeSectionIdx > 0) {
    thirdBlockPerc = Math.abs(highDiff / percPool);
  }

  const firstBlockStyle = useSpring({
    from: { width: "0%" },
    width: `${firstBlockPerc * 100}%`,
    config: { mass: 5, tension: 750, friction: 100 }
  });

  const secondBlockStyle = useSpring({
    from: { width: "100%" },
    width: `${secondBlockPerc * 100}%`,
    config: { mass: 5, tension: 750, friction: 100 }
  });

  const thirdBlockStyle = useSpring({
    from: { width: "0%" },
    width: `${thirdBlockPerc * 100}%`,
    color: activeSectionIdx > 0 ? "#33295c" : "#98d36e",
    config: { mass: 5, tension: 750, friction: 100 }
  });

  let timelineSection = (
    <AgeTimeline>
      <AgeBlock bluish style={firstBlockStyle}>
        {age}
        <br />({roundFull(firstBlockPerc * 100)}%)
      </AgeBlock>
      <AgeBlock style={secondBlockStyle}>
        {roundFull(ageResult)}
        <br />({roundFull(secondBlockPerc * 100)}%)
      </AgeBlock>
      <AgeBlock greenish style={thirdBlockStyle}>
        +{roundFull(highDiff)}
        <br />({roundFull(thirdBlockPerc * 100)}%)
      </AgeBlock>
    </AgeTimeline>
  );

  return (
    <PageWrapper>
      <Wrapper>
        <Left style={leftStyle}>
          <LeftContent style={leftContentStyle}>
            <Logo src={logoSvg} onClick={() => (window.location = "/")} />
            <ResultWrapper ref={sectionRefs[0]}>
              <ResultHeader>
                You've lived {roundFull(sexPredictedAgePerc * 100)}% of your
                life
              </ResultHeader>
              <ResultBlock>
                You'll probably live to be
                <ResultBlockHighlight>
                  {roundFull(sexPredictedAge)} years old
                </ResultBlockHighlight>
              </ResultBlock>
              <ResultBlock>
                You have
                <ResultBlockHighlight>
                  {roundFull(ageResult)} years to live
                </ResultBlockHighlight>
              </ResultBlock>
              <ResultBlock {...sexGreenRedProps}>
                {oppositeSex} your age live
                <ResultBlockHighlight {...sexGreenRedProps}>
                  {oppositeAgeResultDiff > 0
                    ? `${roundFull(oppositeAgeResultDiff)} years less`
                    : `${roundFull(-oppositeAgeResultDiff)} years longer`}
                </ResultBlockHighlight>
              </ResultBlock>
            </ResultWrapper>

            <ResultWrapper ref={sectionRefs[1]}>
              <ResultHeader>
                You live in {loc}.<br /> <br />
                {avgCopy}
              </ResultHeader>
              {Math.abs(parseInt(locResultLowDiff, 10)) > 0 ? (
                <ResultBlock {...locResultLowProps}>
                  {locResultLow[locDataset.specialDim]}
                  <ResultBlockHighlight {...locResultLowProps}>
                    {-locResultLowDiff} years
                  </ResultBlockHighlight>
                </ResultBlock>
              ) : null}
              <ResultBlock>
                {loc}
                <ResultBlockHighlight>
                  {roundFull(num(locResult) + num(age))} years
                </ResultBlockHighlight>
              </ResultBlock>

              <ResultHeader style={{ marginTop: 128 }}>
                These could potentially extend your lifespan:
              </ResultHeader>
              <ResultBlock {...locResultHighProps}>
                {locResultHigh[locDataset.specialDim]}
                <ResultBlockHighlight {...locResultHighProps}>
                  {locResultHighDiff < 0 ? "+" : ""}
                  {-locResultHighDiff} years
                </ResultBlockHighlight>
              </ResultBlock>
              <ResultBlock {...eduResultHighProps}>
                {eduResultHigh[eduDataset.specialDim]}
                <ResultBlockHighlight {...eduResultHighProps}>
                  {eduResultHighDiff < 0 ? "+" : ""}
                  {-eduResultHighDiff} years
                </ResultBlockHighlight>
              </ResultBlock>
            </ResultWrapper>
          </LeftContent>
        </Left>
        <Right>{timelineSection}</Right>
      </Wrapper>
    </PageWrapper>
  );
};

Results.getInitialProps = async ({ query, req }) => {
  if (!query || !Object.keys(query).length) {
    return {};
  }
  const queryStr = Object.keys(query)[0];
  const decoded = Buffer.from(queryStr, "base64").toString();
  const [sex, age, loc, edu] = decoded.split("|");

  const ageNum = parseInt(age, 10);
  const checkAgeRange = ageRange => {
    if (!ageRange.includes("-")) return false;
    const [ageLow, ageHigh] = ageRange.split("-").map(x => parseInt(x, 10));
    return !(ageNum < ageLow || ageNum > ageHigh);
  };

  /* age results */
  const [ageKeys, ageData] = await fetchDataset(ageDataset.file, req);
  const ageResults = ageData.filter(row => {
    // if (row[ageDataset.sexDim] !== sex) return false;
    if (row[ageDataset.ageDim] !== age) return false;
    return true;
  });
  const ageResult = ageResults.find(r => r[ageDataset.sexDim] === sex).Value;
  const ageResultOpposite = ageResults.find(r => r[ageDataset.sexDim] !== sex)
    .Value;

  /* loc results */
  const [locKeys, locData] = await fetchDataset(locDataset.file, req);
  const locResults = locData.filter(row => {
    if (row[locDataset.sexDim] !== sex) return false;
    if (!checkAgeRange(row[locDataset.ageRangeDim])) return false;
    return true;
  });

  const locResult = locResults.find(row => row[locDataset.specialDim] === loc)
    .Value;

  let locResultLow = locResults[0];
  let locResultHigh = locResults[0];
  for (let locResultRow of locResults) {
    const rowVal = parseFloat(locResultRow.Value);
    const lowVal = parseFloat(locResultLow.Value);
    if (rowVal < lowVal) {
      locResultLow = locResultRow;
    }
    const highVal = parseFloat(locResultHigh.Value);
    if (rowVal > highVal) {
      locResultHigh = locResultRow;
    }
  }

  let locResultAvg = locResults.reduce(
    (mem, curr) => mem + parseFloat(curr.Value),
    0
  );
  locResultAvg /= locResults.length;

  /* edu result */
  const [eduKeys, eduData] = await fetchDataset(eduDataset.file, req);
  const eduResults = eduData.filter(row => {
    if (row[eduDataset.sexDim] !== sex) return false;
    if (!checkAgeRange(row[eduDataset.ageRangeDim])) return false;
    return true;
  });

  const eduResult = eduResults.find(row => row[eduDataset.specialDim] === edu)
    .Value;

  let eduResultLow = eduResults[0];
  let eduResultHigh = eduResults[0];
  for (let eduResultRow of eduResults) {
    const rowVal = parseFloat(eduResultRow.Value);
    const lowVal = parseFloat(eduResultLow.Value);
    if (rowVal < lowVal) {
      eduResultLow = eduResultRow;
    }
    const highVal = parseFloat(eduResultHigh.Value);
    if (rowVal > highVal) {
      eduResultHigh = eduResultRow;
    }
  }

  return {
    sex,
    age,
    ageResult,
    ageResultOpposite,
    loc,
    locResult,
    locResultLow,
    locResultHigh,
    locResultAvg,
    edu,
    eduResult,
    eduResultLow,
    eduResultHigh
  };
};

export default Results;
