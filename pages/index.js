import React, { useState } from "react";
import PageWrapper from "../components/PageWrapper";
import { fetchDataset, datasets } from "../lib/data";

import logoSvg from "../assets/Logo_White.svg";
import heroSvg from "../assets/Hero.svg";
import ageSvg from "../assets/Age.svg";
import sexSvg from "../assets/Sex.svg";
import locSvg from "../assets/Location.svg";
import eduSvg from "../assets/Education.svg";
import styled from "@emotion/styled";
import SelectButton from "../components/SelectButton";
import ResultsButton from "../components/ResultsButton";

const PageBlock = styled.div`
  flex: 1 1 auto;
  align-self: stretch;
  height: 50vh;
  width: 100vw;
`;

const Top = styled(PageBlock)`
  position: fixed;
  top: -50vh;
  left: -50vw;
  right: -50vw;
  width: 200vw;
  height: 100vh;
  padding: 80px calc(50vw + 80px);
  padding-top: calc(50vh + 80px);
  bottom: 50%;
  background-color: #33295c;
  color: white;
  display: flex;
  flex-direction: column;
  z-index: -1;
`;

const Bottom = styled(PageBlock)`
  margin-top: 50vh;
  background-color: white;
  color: #33295c;
  flex-direction: row;
  padding: 80px;
  padding-right: 0;
  z-index: 1;
`;

const ButtonsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-shrink: 0;
  &:after {
    content: "";
    height: 1px;
    width: 80px;
    flex-shrink: 0;
    display: block;
  }
`;

const Logo = styled.img`
  flex-shrink: 0;
  width: 196px;
  height: 42px;
`;

const HeroImage = styled.img`
  position: absolute;
  bottom: 0;
  right: calc(50vw - 64px);
  max-height: calc(50vh - 48px);
  z-index: -1;
  width: 100vh;
`;

const Guide = styled.div`
  flex: 1 1 auto;
  font-size: 24px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;

  margin-top: 48px;
  max-width: 640px;

  > p {
    margin-top: 24px;
    &:first-of-type {
      margin-top: 0;
    }
  }
`;

const Index = ({ ageValues, locValues, eduValues }) => {
  const [sex, setSex] = useState(null);
  const sexOptions = [
    { value: "Mehed", label: "Male" },
    { value: "Naised", label: "Female" }
  ];

  const [age, setAge] = useState(null);
  const ageOptions = [...new Array(85)].map((_, idx) => ({
    value: idx + 1,
    label: idx + 1
  }));

  const [loc, setLoc] = useState(null);
  const locOptions = locValues.map(locValue => ({
    value: locValue,
    label: locValue
  }));

  const [edu, setEdu] = useState(null);
  const eduOptions = eduValues.map(eduValue => ({
    value: eduValue,
    label: eduValue
  }));

  const serializedResults = Buffer.from(
    [sex, age, loc, edu]
      .filter(opt => opt)
      .map(opt => opt.value)
      .join("|")
  ).toString("base64");

  return (
    <PageWrapper>
      <Top>
        <Logo src={logoSvg} />
        <Guide>
          <p>
            Minust is a tool to visualise yourself in the context of Estonian
            statistical demographics.
          </p>
          <p>
            Get insights on how your possible life expectancy compares to that
            of an average Estonian; how to (probably) live longer; where your
            dating chances are the best and much more.
          </p>
          {/*<p>Enter your details below to get started:</p>*/}
        </Guide>
        <HeroImage src={heroSvg} />
      </Top>
      <Bottom>
        <ButtonsWrapper>
          <SelectButton
            options={sexOptions}
            onSelect={setSex}
            label="Gender"
            value={sex}
            iconPath={sexSvg}
          />
          <SelectButton
            options={ageOptions}
            onSelect={setAge}
            label="Age"
            value={age}
            iconPath={ageSvg}
          />
          <SelectButton
            options={locOptions}
            onSelect={setLoc}
            label="Location"
            value={loc}
            iconPath={locSvg}
          />
          <SelectButton
            options={eduOptions}
            onSelect={setEdu}
            label="Education"
            value={edu}
            iconPath={eduSvg}
          />
          <ResultsButton disabled={!age || !sex} results={serializedResults} />
        </ButtonsWrapper>
      </Bottom>
    </PageWrapper>
  );
};

Index.getInitialProps = async ({ req }) => {
  let AGE_VALUES = [];
  let LOC_VALUES = [];
  let EDU_VALUES = [];

  for (let dataset of datasets) {
    const [keyLabels, data] = await fetchDataset(dataset.file, req);
    data.forEach(item => {
      AGE_VALUES.push(item[dataset.ageRangeDim]);
      if (dataset.file === "RV0452") {
        LOC_VALUES.push(item[dataset.specialDim]);
      } else if (dataset.file === "RV0454") {
        EDU_VALUES.push(item[dataset.specialDim]);
      }
    });
    // console.log(dataset, keyLabels, data);
  }

  // console.log({ AGE_VALUES, LOC_VALUES, EDU_VALUES });

  AGE_VALUES = [...new Set(AGE_VALUES)];
  LOC_VALUES = [...new Set(LOC_VALUES)];
  EDU_VALUES = [...new Set(EDU_VALUES)];

  return {
    ageValues: AGE_VALUES,
    locValues: LOC_VALUES,
    eduValues: EDU_VALUES
  };
};

export default Index;
